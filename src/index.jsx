import React from 'react'
import ReactDOM from 'react-dom'
// import { observable } from 'mobx-react'
import App from './App'
import 'antd/dist/antd.css'

// const store = observable({ articles: [] })
ReactDOM.render(<App />, document.getElementById('app'))
