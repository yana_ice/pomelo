import axios from 'axios'

const API_KEY = 'E3PPQGxTckV1OXKtm8VJLpG2yODq5IhF'
const baseURL = 'https://api.nytimes.com/svc'

const instance = axios.create({
  baseURL,
})

const apiService = {
  getMostPopular: () =>
    instance.get(`mostpopular/v2/viewed/1?api-key=${API_KEY}`),
  searchBy: (query, page) =>
    instance.get(
      `search/v2/articlesearch.json?api-key=${API_KEY}&${query}&page=${page}`
    ),
}

export default apiService
