import React, { Component } from 'react'
import styled from 'styled-components'
import get from 'lodash/get'
import moment from 'moment'
import { observer, inject } from 'mobx-react'
import { toJS } from 'mobx'
import { Link } from 'react-router-dom'
import { Tag, Card, Divider, Icon } from 'antd'
import Layout from './layout'
import { Title } from '../components/typography'

const ArticleContainer = styled.div`
  display: flex;
  flex-flow: row wrap;
  margin-top: 20px;

`

const Article = styled(Card)`
  width: 238px;
  margin: 10px !important;
`

class HomePage extends Component {
  constructor(props) {
    super(props)
  }

  componentDidMount() {
    this.getMostarticleArticles()
  }

  getMostarticleArticles = async () => {
    await this.props.article.fetchArticles()
  }

  getPhotoArticle = article => {
    const imageType = article.media.find(media => media.type === 'image')
    return get(imageType, 'media-metadata[0]url')
  }

  render() {
    const { articles, loading } = this.props.article
    return (
      <Layout {...this.props}>
        <ArticleContainer>
          {loading && <Icon type="loading" width="100" />}
          {articles.map(article => (
            <Link to={`/article/${article.id}`} key={article.id}>
              <Article title={article.section}>
                <div>
                  <Title>{article.title}</Title>
                  <Divider />
                  <Tag color="black">
                    {moment(article.published_date).fromNow()}
                  </Tag>
                </div>
              </Article>
            </Link>
          ))}
        </ArticleContainer>
      </Layout>
    )
  }
}

export default inject('article')(observer(HomePage))
