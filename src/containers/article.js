import React, { Component } from 'react'
import styled from 'styled-components'
import get from 'lodash/get'
import last from 'lodash/last'
import moment from 'moment'
import { observer, inject } from 'mobx-react'
import { toJS } from 'mobx'
import { Link } from 'react-router-dom'
import { Tag, Card, Divider, Icon } from 'antd'
import Layout from './layout'
import { Header2 } from '../components/typography'

const ArticleContainer = styled.div`
  display: flex;
  flex-flow: row wrap;
  margin-top: 20px;
  height: 100%;
  margin-bottom: 280px;
`

const Content = styled.div`
  font-size: 14px;
  line-height: 1.5;
  display: flex;
  flex-flow: column;
  justify-content: space-between;
`
const Context = styled.div`
  flex: 1;
`

const CoverImg = styled.img`
  margin-top: 40px;
  margin-bottom: 40px;
  width: 100%;
`
const Footer = styled.div`
  font-size: 12px;
  color: gray;
`
class ArticlePage extends Component {
  constructor(props) {
    super(props)
  }

  componentDidMount() {
    
  }

  getPhotoArticle = article => {
    let imageType = article.media.find(media => media.type === 'image')
    imageType = last(imageType['media-metadata'])
    return get(imageType, 'url')
  }

  render() {
    const { articleId } = this.props.match.params
    let articleDetail = toJS(this.props.article.articles)
    articleDetail = articleDetail.find(ar => ar.id +'' === articleId + '')
    return (
      <Layout {...this.props}>
        <ArticleContainer>
          <Header2>{get(articleDetail, 'title')}</Header2>
          <CoverImg src ={this.getPhotoArticle(articleDetail)} />
          <Content>
            <Context>{get(articleDetail, 'abstract')}</Context>
            <Footer>{`Publish Date: ${get(articleDetail, 'published_date')}`}</Footer>
          </Content>
        </ArticleContainer>
      </Layout>
    )
  }
}

export default inject('article')(observer(ArticlePage))
