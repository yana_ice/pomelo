import React, { Component } from 'react'
import styled from 'styled-components'
import { Link } from 'react-router-dom'
import { Layout, Menu, Breadcrumb, Input, Typography } from 'antd'
import { Header1 } from '../components/typography'

const { Footer } = Layout
const MainLayout = styled.div`
  /* display: flex;
  flex-flow: column;
  height: 100%; */
`
const Body = styled.div`
  height: 100%;
`
const Header = styled.div`
  height: 80px;
  background-color: black;
  display: flex;
  align-items: center;
  padding: 20px;
`
const Content = styled.div`
  padding: 0 40px;
`
const Search = styled(Input.Search)`
  margin-left: 20px !important;
  width: 250px !important;
`
class LayoutPage extends Component {
  constructor(props) {
    super(props)
    this.state = {}
  }

  render() {
    return (
      <MainLayout>
        <Header>
          <Header1>NYTimes</Header1>
          <Search
            placeholder="search article"
            onSearch={value => console.log(value)}
          />
        </Header>
        <Content>
          <Breadcrumb style={{ margin: '16px 0' }}>
            <Link to='/'><Breadcrumb.Item>HOME</Breadcrumb.Item></Link>
            { this.props.location && this.props.location.pathname.split('/').map(menu => <Breadcrumb.Item>{menu && menu.toUpperCase()}</Breadcrumb.Item>) }
          </Breadcrumb>
          <Body>{this.props.children}</Body>
        </Content>
        <Footer style={{ textAlign: 'center' }}>Ant Design ©2018 Created by Ant UED</Footer>
      </MainLayout>
    )
  }
}

export default LayoutPage
