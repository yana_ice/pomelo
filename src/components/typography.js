import styled from 'styled-components'
import moment from 'moment'

const Header1 = styled.div`
  font-size: 27px;
  color: white;
`
const Header2 = styled.div`
  font-size: 37px;
  color: black;
`
const Title = styled.div`
  font-size: 19px;
  font-weight: bold;
  color: black;
`
const Datetime = styled.div`
  font-size: 12px;
  color: gray;
`

export {
  Header1,
  Header2,
  Title,

}