import React from 'react'
import PropTypes from 'prop-types'
import styled from 'styled-components'

const Container = styled.div`
  background: green;
`

const HomePage = () => <Container />

HomePage.propTypes = {
  title: PropTypes.string,
}

export default HomePage
