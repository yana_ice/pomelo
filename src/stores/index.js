import Article from './article'

let instance

const initStore = () => ({
  article: new Article(),
})

export const getStores = () => {
  if (!instance) {
    instance = initStore()
  }
  return instance
}
