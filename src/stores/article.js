import { observable, action, computed } from 'mobx'
import nyTimeApi from '../services/nytimesApi'

export default class Article {
  @observable articles = []
  @observable loading = false

  @action
  async fetchArticles () {
    this.loading = true
    const resp = await nyTimeApi.getMostPopular()
    this.articles = resp.data.results
    this.loading = false
  }

  @action
  updateArticle(data) {
    this.articles = data
  }

  @action
  addArticle(data) {
    this.articles.push(data)
  }
}
