import React, { Component } from 'react'
import { hot } from 'react-hot-loader/root'
import { BrowserRouter as Router, Route, IndexRoute } from 'react-router-dom'
import { Provider } from 'mobx-react'
import HomePage from './containers/homepage'
import Article from './containers/article'
import Layout from './containers/layout'
import { getStores } from './stores'

class App extends Component {
  constructor(props) {
    super(props)
    this.state = {}
  }

  render() {
    return (
      <Provider {...getStores()}>
        <Router>
          <Route exact path="/" component={HomePage} />
          <Route exact path="/article/:articleId" component={Article} />
        </Router>
      </Provider>
    )
  }
}

export default hot(App)
